import React, { Component } from 'react';
import './App.css';
import queryString from 'query-string';
import {connect} from 'react-redux';

class Auth extends Component {
  componentWillMount() {
    const hash = this.props.location.hash;
    const parsedHash = queryString.parse(hash);
    this.props.onLogin(parsedHash.access_token);
    this.props.history.push(`/browse/?token=${parsedHash.access_token}&path=/`);
  }
    render() {
        return (<div>loading</div>);
    }
}
const mapStateToProps = (state) => ({
  token: state.get('token'),
});
const mapDispatchToProps = dispatch => ({
  onLogin: (token) => dispatch({type: 'LOGIN', payload: {token}})
});

export default connect(mapStateToProps, mapDispatchToProps)(Auth);
