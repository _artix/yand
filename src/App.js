import React, { Component } from 'react';
import logo from './logo.png';
import './App.css';
import { Switch, Route, BrowserRouter} from 'react-router-dom';
import Auth from './Auth';
import Browse from './Browse';
import { Navbar } from 'reactstrap';
import {connect} from 'react-redux';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar>
          {!this.props.token && <a href="https://oauth.yandex.ru/authorize?response_type=token&client_id=135947face574b2e9cef11235beafeaf">
            Login
          </a>}
          {this.props.token && <a href="/" onClick={this.props.onLogout}>Logout</a>}
        </Navbar>
        {!this.props.token && <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Яндекс диииск</h2>
        </div>}
        <BrowserRouter>
          <Switch>
            <Route exact path='/browse' component={Browse}/>
            <Route path='/auth' component={Auth}/>
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  token: state.get('token'),
});

const mapDispatchToProps = dispatch => ({
  onLogout: () => dispatch({type: 'LOGOUT'}),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
