import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Table, Button, Nav, NavItem } from 'reactstrap';
import './Browse.css';
import dirImage from './dir.png';
import queryString from 'query-string';

class Browse extends Component {
  componentWillMount() {
    const search = this.props.location.search;
    const parsedSearch = queryString.parse(search);

    // при заходе по ссылке в стейте ещё нет токена
    if (this.props.token) {
      this.props.onLoadDir(this.props.token, this.props.path);
    } else if (parsedSearch.token) {
      this.props.onLogin(parsedSearch.token)
    }
  }

  componentWillReceiveProps(nextProps) {
    // второй шаг захода по ссылке
    if (nextProps.token && !this.props.token) {
      const search = this.props.location.search;
      const parsedSearch = queryString.parse(search);
      const path = parsedSearch.path || this.props.path;
      this.props.onLoadDir(nextProps.token, path);
    }

    // меняется директория меняется url
    if (this.props.token && (nextProps.path !== this.props.path)) {
      this.props.history.push(`/browse/?token=${this.props.token}&path=${nextProps.path}`);
    }
  }

  getUp(path) {
    return path.substring(0, path.lastIndexOf('/') + 1);
  }

  render() {
    const columns = this.props.items.map((one, i) => {
      if (one.type === 'dir') {
        return (
          <tr key={i}>
            <td><img alt="dir" className="folder" src={dirImage}/><Button color="link" onClick={() => this.props.onLoadDir(this.props.token, this.props.path + one.name)}>{one.name}</Button></td>
            <td>{one.size}</td>
          </tr>
        );
      }
      return (
        <tr key={i}>
          <td>{one.name}</td>
          <td>{one.size}</td>
        </tr>
      );
    });

    return (
      <div>
        <Nav>
          <NavItem>
            <Button color="primary" onClick={() => this.props.onLoadDir(this.props.token, this.getUp(this.props.path))} disabled={this.props.path === '/'}>
              Вверх
            </Button>
          </NavItem>
          <NavItem>
            <span className="path">{this.props.path}</span>
          </NavItem>
        </Nav>
        <Table>
          <thead>
          <tr>
            <th>Имя</th>
            <th>Размер</th>
          </tr>
          </thead>
          <tbody>
          {columns}
          </tbody>
        </Table>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  token: state.get('token'),
  path: state.get('path'),
  items: state.get('items'),
});
const mapDispatchToProps = dispatch => ({
  onLoadDir: (token, path) => dispatch({type: 'LOAD DIR', payload: {token, path}}),
  onLogin: (token) => dispatch({type: 'LOGIN', payload: {token}}),
});

export default connect(mapStateToProps, mapDispatchToProps)(Browse);

