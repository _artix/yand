import { ajax } from 'rxjs/observable/dom/ajax';
import { combineEpics } from 'redux-observable';
import 'rxjs';


const loadDirEpic = action$ =>
  action$
    .ofType('LOAD DIR')
    .mergeMap(action =>
      ajax
        .getJSON(`https://cloud-api.yandex.net/v1/disk/resources?path=${action.payload.path}`, {Authorization: `OAuth ${action.payload.token}`})
        .map(payload => ({type: 'LOAD DIR FULFILLED', payload}))
    );

const rootEpic = combineEpics(
  loadDirEpic
);

export {rootEpic};