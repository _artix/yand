import { createStore, applyMiddleware } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { composeWithDevTools } from 'remote-redux-devtools';
import { reducer, initialState } from './reducers';
import {rootEpic} from './epics';


const epicMiddleware = createEpicMiddleware(rootEpic);
const enhancer = composeWithDevTools(applyMiddleware(epicMiddleware));

const store = createStore(
  reducer,
  initialState,
  enhancer
);

export default store;
