import { Map } from 'immutable';

const initialState = new Map({
  token: null,
  path: '/',
  items: []
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD DIR FULFILLED':
      let path = action.payload.path;
      path = path.substring(path.indexOf(':') + 1);
      const items = action.payload._embedded.items.map(one => ({name: one.name,size: one.size,type: one.type}));
      return state.merge(Map({path, items}));
    case 'LOGIN':
      return state
        .set('token', action.payload.token);
    case 'LOGOUT':
      return initialState;
    default:
      return state;
  }
};

export {initialState, reducer};